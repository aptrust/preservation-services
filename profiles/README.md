# Profiles

This directory contains BagIt profiles used by the validator during
ingest, and a pronom signature file (default.sig) that siegfried uses
to identify file formats.
