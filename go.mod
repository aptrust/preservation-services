module github.com/APTrust/preservation-services

go 1.13

require (
	github.com/go-redis/redis/v7 v7.4.1
	github.com/google/uuid v1.3.0
	github.com/minio/minio-go/v7 v7.0.15
	github.com/mitchellh/go-ps v1.0.0
	github.com/nsqio/go-nsq v1.0.8
	github.com/nsqio/nsq v1.2.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/richardlehane/siegfried v1.9.1
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
)
